# ifndef PROGRAM_INCLUDE_BEAM_SPLITTER_H_
# define PROGRAM_INCLUDE_BEAM_SPLITTER_H_

# include "netxpto_20190816.h"


// Implements a Balanced BeamSplitter
class BeamSplitter : public Block {

	bool firstTime{ true };

public:

	double outputOpticalWavelength{ 1550e-9 };
	double outputOpticalFrequency{ SPEED_OF_LIGHT / outputOpticalWavelength };
	t_real F = 1 / sqrt(2);
	t_complex unit = 1;
	std::array <t_complex, 4> matrix = { { F*unit, F*unit, F*unit, -unit*F } };
	t_real mode = 0 ;

	BeamSplitter() {};
	BeamSplitter(std::initializer_list<Signal *> InputSig, std::initializer_list<Signal *> OutputSig) :Block(InputSig, OutputSig) {};

	void initialize(void);
	bool runBlock(void);

	void setTransferMatrix(std::array<std::complex<double>, 4> TransferMatrix) { matrix = TransferMatrix; }
	std::array<std::complex<double>, 4> const getTransferMatrix(void) { return matrix; }

	void setMode(t_real Mode) { mode = Mode; }

private:

};


#endif // !PROGRAM_INCLUDE_BEAM_SPLITTER_H_

