# ifndef PROGRAM_INCLUDE_DECODER_H_
# define PROGRAM_INCLUDE_DECODER_H_

# include "netxpto_20190816.h"


// Implements a IQ Decoder.
class Decoder : public Block {

	/* Input Parameters */

	t_integer m{ 4 };

	std::vector<t_complex> iqAmplitudes{ { 1.0, 1.0 },{ -1.0, 1.0 },{ -1.0, -1.0 },{ 1.0, -1.0 } };

	/* State Variables */

	

public:

	/* Methods */
	Decoder() {};
	Decoder(std::initializer_list<Signal *> InputSig, std::initializer_list<Signal *> OutputSig) :Block(InputSig, OutputSig) {};

	void initialize(void);
	bool runBlock(void);

	void setM(int mValue);
	int getM() { return m; };

	void setIqAmplitudes(std::vector<t_iqValues> iqAmplitudesValues);
	std::vector<t_iqValues> getIqAmplitudes() { return iqAmplitudes; }

	/*void setIqValues(vector<t_complex> iq) { iqValues = iq; };
	vector<t_complex> getIqValues() { return iqValues; }*/

};

# endif
