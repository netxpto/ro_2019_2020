# ifndef PROGRAM_INCLUDE_FIBER_H_
# define PROGRAM_INCLUDE_FIBER_H_

# include "netxpto_20190816.h"

# include "../algorithms/fft/fft_20180208.h"


class Fiber : public Block {

private:

	/* Input Parameters */
	// following are the parameters for the fiber
	t_real fiberLength{ 50.0 };                  // fiber length (km)
	t_real attenuationCoeficient{ 0.2};          // attenuation (dB/km)
	t_real dispersionCoeficient{ 16.0 };         // group-velocity dispersion parameter (ps/km/nm)
	t_real dispersionSlope{ 0.055 };             // dispersion slope (ps/km/nm^2) (f)
	t_real nlCoeff{ 0.0013 };                    // non-linear coeffient (1/W/m)
	bool   useBeta3{ false };                    // use polynomail expression for beta3
	bool   useBeta4{ false };                    // use polynomail expression for beta4

	/* State Variables */
	// following are the global simulation variables
	t_real centralWavelength{ 1550 };            // central wavelength (nm)

	// following are the parameters for the SSFM
	t_real stepLength{ 100.0 };                  // step length (m) 
	int maxIter{ 4 };                            // maximum iterations
	double tol{ 1e-5 };                          // tolerence

	// folowwing are the parameters for handling data in SSFM
	std::vector<t_complex> previousCopy;
	t_real saveInterval{ fiberLength };          // step length (km)
	bool saveData{ false };                      // save data after given interval
	int K{ 0 };
	int L{ 0 };

	// linear effect vector (attenuation + chromatic dispersion)
	std::vector<t_complex> linear_effect_half_step;   // linear operator to apply att + distortion effect

public:

	/* Methods */
	Fiber(std::initializer_list<Signal*> InputSig, std::initializer_list<Signal*> OutputSig) :Block(InputSig, OutputSig) {};

	void initialize(void); 
	bool runBlock(void); 

	void setFiberLength(t_real newFiberLength) { fiberLength = newFiberLength; }
	t_real getFiberLength(void) { return fiberLength; }

	void setStepLength(t_real stepLength_) { stepLength = stepLength_; }
	t_real getStepLength(void) { return stepLength; }

	void setSaveInterval(t_real saveInterval_) { saveInterval = saveInterval_; }
	t_real getSaveInterval(void) { return saveInterval; }

	void setAttenuationCoeficient(t_real alpha) { attenuationCoeficient = alpha; }
	t_real getAttenuationCoeficient() { return attenuationCoeficient; }

	void setDispersionCoeficient(t_real beta2) { dispersionCoeficient = beta2; }
	t_real getDispersionCoeficient() { return dispersionCoeficient; }

	void setDispersionSlope(t_real dSlope) { dispersionSlope = dSlope; }
	t_real getDispersionSlope() { return dispersionSlope; }

	void setNlCoeff(t_real nonliearCoeff) { nlCoeff = nonliearCoeff; }
	t_real getNlCoeff() { return nlCoeff; }

	void setSaveData(bool svData) { saveData = svData; }
	bool getSaveData() { return saveData; }
};

# endif