# ifndef PROGRAM_INCLUDE_LINEAR_EQUILIZER_H_
# define PROGRAM_INCLUDE_LINEAR_EQUILIZER_H_

# include "netxpto_20190816.h"

# include "../algorithms/fft/fft_20180208.h"


class LinearEqualizer : public Block {

private:

	/* Input Parameters */
	// following are the parameters for the fiber
	t_real fiberLength{ 50.0 };                  // fiber length (km)
	t_real dispersionCoeficient{ 16.0 };         // group-velocity dispersion parameter (ps/km/nm)

	/* State Variables */
	// following are the global simulation variables
	t_real centralWavelength{ 1550 };            // central wavelength (nm)


	// folowwing are the parameters for handling data in SSFM
	std::vector<t_complex> previousCopy;
	int K{ 0 };
	int L{ 0 };

	// CDE transfer function 
	std::vector<t_complex> cde; 

public:

	/* Methods */
	LinearEqualizer(std::initializer_list<Signal*> InputSig, std::initializer_list<Signal*> OutputSig) :Block(InputSig, OutputSig) {};

	void initialize(void); 
	bool runBlock(void); 

	void setFiberLength(t_real newFiberLength) { fiberLength = newFiberLength; }
	t_real getFiberLength(void) { return fiberLength; }

	void setDispersionCoeficient(t_real beta2) { dispersionCoeficient = beta2; }
	t_real getDispersionCoeficient() { return dispersionCoeficient; }


};

# endif