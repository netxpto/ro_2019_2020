# ifndef PROGRAM_LOAD_FROM_FILE_H_
# define PROGRAM_LOAD_FROM_FILEI_H_

# include "netxpto_20190816.h"

class LoadFromFile : public Block { 

public: 

	// ##########################################################################

	LoadFromFile(std::initializer_list<Signal *> InputSig, std::initializer_list<Signal *> OutputSig) :Block(InputSig, OutputSig) {};

	void initialize(void);
	bool runBlock(void);
	void terminate(void);

	// ##########################################################################

	void setSamplingPeriod(double sPeriod) { symbolPeriod; };
	double const getSamplingPeriod(void) { return samplingPeriod; };

	void setSymbolPeriod(double sPeriod) { symbolPeriod = sPeriod; };
	double const getSymbolPeriod(void) { return symbolPeriod; };

	void setFileName(std::string sFileName) { fileName = sFileName; };
	std::string const getFileName(void) { return fileName; };

	// ##########################################################################
	
private:

	// # Input Parameters ########################################################

	std::string fileName{ "InputFile.txt" };

	double samplingPeriod{ 1 };
	double symbolPeriod{ 1 };

	// # State Variables #########################################################

	std::ifstream inFile;

	// # Private Functions ########################################################

	void processHeader();
};

# endif
