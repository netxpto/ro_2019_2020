# ifndef MESSAGE_SOURCE_H_
# define MESSAGE_SOURCE_H_

# include "netxpto_20190816.h"

enum class MessageSourceMode { OneByOne, Repeat };

class MessageSource : public Block {

public:

	//##############################################################################################################

	MessageSource(std::initializer_list<Signal *> InputSig, std::initializer_list<Signal *> OutputSig) : Block(InputSig, OutputSig) {};

	bool runBlock(void);

	//##############################################################################################################

	void setMode(MessageSourceMode m) { mode = m; }
	MessageSourceMode getMode(void) const { return mode; };

	void setNumberOfMessages(unsigned long nOfMessages) { numberOfMessages = nOfMessages; }
	unsigned long getNumbernOfMessages() { return numberOfMessages; }

	//###############################################################################################################

private:

	//# Input Parameters ############################################################################################

	int long numberOfMessages{ -1 };
	MessageSourceMode mode{ MessageSourceMode::OneByOne };

	//# State Variables #############################################################################################

	t_message message;
	
	 //###############################################################################################################

};

# endif


