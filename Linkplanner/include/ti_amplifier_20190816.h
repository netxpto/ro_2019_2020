# ifndef TI_AMPLIFIER_H_
# define TI_AMPLIFIER_H_

# include "netxpto_20190816.h"

# include "white_noise_20190816.h"
# include "add_20190816.h"
# include "ideal_amplifier_20190816.h"
# include "pulse_shaper_20190816.h" 


class TiAmplifier : public SuperBlock {

public:

	//##############################################################################################################

	TiAmplifier(std::initializer_list<Signal *> inputSignals, std::initializer_list<Signal *> outputSignals);
	TiAmplifier(std::initializer_list<Signal*> inputSignals, std::initializer_list<Signal*> outputSignals, std::string sFolderName);

	void initialize(void);
	bool runBlock(void);

	//##############################################################################################################

	void setInputReferredNoisePowerSpectralDensity(double newInputReferredNPDS) { WhiteNoise_.setNoiseSpectralDensity(newInputReferredNPDS); }
	double const getInputReferredNoisePowerSpectralDensity(void) { WhiteNoise_.getNoiseSpectralDensity(); }

	void setNoiseSamplingPeriod(t_real samplingPeriod) { WhiteNoise_.setSamplingPeriod(samplingPeriod); }
	t_real getNoiseSamplingPeriod(void) { return WhiteNoise_.getSamplingPeriod(); };

	void setNoiseSymbolPeriod(t_real symPeriod) { WhiteNoise_.setSymbolPeriod(symPeriod); }
	t_real getNoiseSymbolPeriod(void) { return WhiteNoise_.getSymbolPeriod(); };

	void setGain(double ga) { IdealAmplifier_.setGain(ga); };
	double const getGain(void) { IdealAmplifier_.getGain(); }

	void setFilterType(pulse_shaper_filter_type filterType) { ElectricalFilter_.setFilterType(filterType); };
	pulse_shaper_filter_type getFilterType(void) { return ElectricalFilter_.getFilterType(); };

	//void setCutoffFrequency(double cutoffFreq) { ElectricalFilter_.setCutoffFrequency(cutoffFreq); };
	//double const getCutoffFrequency(void) { return ElectricalFilter_.getCutoffFrequency(); };

	void setTiAmplifierImpulseResponseTimeLength(int impResponseTimeLength) { ElectricalFilter_.setImpulseResponseTimeLength_symbolPeriods(impResponseTimeLength); }
	int const getTiAmplifierImpulseResponseTimeLength(void) { return ElectricalFilter_.getImpulseResponseTimeLength_symbolPeriods(); };

	//void setImpulseResponse(std::vector<t_real> ir) { ElectricalFilter_.setImpulseResponse(ir); };
	//std::vector<t_real> getImpulseResponse(void) { ElectricalFilter_.getImpulseResponse(); }

	void setImpulseResponseFilename(std::string fName) { ElectricalFilter_.setImpulseResponseFilename(fName); }
	std::string getImpulseResponseFilename(void) { ElectricalFilter_.getImpulseResponseFilename(); }

	void setImpulseResponseLength(int impResponseLength) { ElectricalFilter_.setImpulseResponseLength(impResponseLength); };
	int const getImpulseResponseLength(void) { return ElectricalFilter_.getImpulseResponseLength(); };

	void setSeeBeginningOfImpulseResponse(bool sBegginingOfImpulseResponse) { ElectricalFilter_.setRemoveFilterDelay(sBegginingOfImpulseResponse); };
	double const getSeeBeginningOfImpulseResponse(void) { return ElectricalFilter_.getRemoveFilterDelay(); };

	void setTiAmplifierPassiveFilterMode(bool passiveFiltMode) { ElectricalFilter_.setPassiveFilterMode(passiveFiltMode); };
	double const getTiAmplifierPassiveFilterMode(void) { return ElectricalFilter_.getPassiveFilterMode(); }

	void setTiAmplifierRollOffFactor(double rollOffFactor) { ElectricalFilter_.setRollOffFactor(rollOffFactor); };
	double const getTiAmplifierRollOffFactor(void) { return ElectricalFilter_.getRollOffFactor(); };

	//###############################################################################################################

private:

	// #####################################################################################################
	// ################################## Input Parameters #################################################
	// #####################################################################################################

	std::string signalsFolderName{ "SuperBlock_TiAmplifier" };
	bool logValue{ true };
	std::string logFileName{ "SuperBlock_TiAmplifier.txt" };

	// #####################################################################################################
	// ################## Internal Signals Declaration and Inicialization ##################################
	// #####################################################################################################

	TimeContinuousAmplitudeContinuousReal ElectricalSignal_In{ "S0_ElectricalSignal_In.sgn" };
	TimeContinuousAmplitudeContinuousReal WhiteNoiseOut{ "S1_WhiteNoiseOut.sgn" }; // Amplified
	TimeContinuousAmplitudeContinuousReal AddOut{ "S2_AddOut.sgn" }; // Noise
	TimeContinuousAmplitudeContinuousReal IdealAmplifierOut{ "S3_IdealAmplifierOut.sgn" }; // Noisy Amplified Signal
	TimeContinuousAmplitudeContinuousReal ElectricalSignal_Out{ "S4_ElectricalFilterOut.sgn" }; // Filtered Noisy Signal

	// #####################################################################################################
	// ########################### Blocks Declaration and Inicialization ###################################
	// #####################################################################################################

	WhiteNoise WhiteNoise_{ {},{&WhiteNoiseOut} };
	Add Add_{ {&ElectricalSignal_In, &WhiteNoiseOut},{ &AddOut } };
	IdealAmplifier IdealAmplifier_{ { &AddOut },{ &IdealAmplifierOut } };
	PulseShaper ElectricalFilter_{ { &IdealAmplifierOut }, { &ElectricalSignal_Out } };

};


#endif
