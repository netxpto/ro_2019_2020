#include "beam_splitter_20190816.h"


void BeamSplitter::initialize(void) {

	outputSignals[0]->setSymbolPeriod(inputSignals[0]->getSymbolPeriod());
	outputSignals[0]->setSamplingPeriod(inputSignals[0]->getSamplingPeriod());
	outputSignals[0]->setFirstValueToBeSaved(inputSignals[0]->getFirstValueToBeSaved());
	outputSignals[0]->setCentralWavelength(inputSignals[0]->getCentralWavelength());
	outputSignals[0]->setCentralFrequency(inputSignals[0]->getCentralFrequency());

	outputSignals[1]->setSymbolPeriod(inputSignals[0]->getSymbolPeriod());
	outputSignals[1]->setSamplingPeriod(inputSignals[0]->getSamplingPeriod());
	outputSignals[1]->setFirstValueToBeSaved(inputSignals[0]->getFirstValueToBeSaved());
	outputSignals[1]->setCentralWavelength(inputSignals[0]->getCentralWavelength());
	outputSignals[1]->setCentralFrequency(inputSignals[0]->getCentralFrequency());

}

bool BeamSplitter::runBlock(void) {

	int ready{ 0 };
	if (mode == 0)
		ready = inputSignals[0]->ready();
	else
	{
		int ready0 = inputSignals[0]->ready();
		int ready1 = inputSignals[1]->ready();
		ready = std::min(ready0, ready1);
	};

	
	int space1 = outputSignals[0]->space();
	int space2 = outputSignals[1]->space();
	int space = std::min(space1, space2);

	int process = std::min(ready, space);

	if (process == 0) return false;
	

	for (int i = 0; i < process; i++) {

		if (mode == 0) {

			t_complex inSignal1;
			//This should implement a 1x2 beam splitter

				inputSignals[0]->bufferGet(&inSignal1);

				t_complex outSignal1 = matrix[0] * inSignal1;
				t_complex outSignal2 = matrix[2] * inSignal1;

				outputSignals[0]->bufferPut(outSignal1);
				outputSignals[1]->bufferPut(outSignal2);

			}

		else {

			t_complex inSignal1;
			t_complex inSignal2;
		
			std::complex<t_real> imaginary(0, 1);

			//This should implement a 2x2 beam splitter

				inputSignals[0]->bufferGet(&inSignal1);
				inputSignals[1]->bufferGet(&inSignal2);

				t_complex out1 = matrix[0] * inSignal1 + matrix[1] * inSignal2;
				t_complex out2 = matrix[2] * inSignal1 + matrix[3] * inSignal2;

				outputSignals[0]->bufferPut(out1);
				outputSignals[1]->bufferPut(out2);

			
		}
	

	}
	return true;
};
