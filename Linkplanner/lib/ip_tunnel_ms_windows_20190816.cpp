# include "ip_tunnel_ms_windows_20190816.h" 

void IPTunnel::initialize(void)
{
	// Initialize the Server 
	// The server is assumed to be the block without input signals,
	// i.e. the block that is going to receive the data.
	if (inputSignals.empty()) {
		if (transportProtocol == transport_protocol::TCP) {
			std::cerr << "TCP Server" << std::endl;
			if (!server_tcp()) std::cerr << "Error opening TCP server\n";
		}
		else {
			std::cerr << "UDP Server" << std::endl;
			if (!server_udp()) std::cerr << "Error opening UDP server\n";
		}
		// Resize the local buffer
		if (outputSignals[0]->getValueType() == (signal_value_type::t_message))
			ip_tunnel_buffer.resize(outputSignals[0]->getBufferLength()*MAX_MESSAGE_SIZE);
		else
			ip_tunnel_buffer.resize(outputSignals[0]->getBufferLength()*outputSignals[0]->valueTypeSizeOf() / sizeof(char));
	}
	// Initialize the Client
	// The client is assumed to be the block without output signals,
	// i.e. the block that is going to transmit the data.
	else { 
		if (transportProtocol == transport_protocol::TCP) {
			std::cerr << "TCP Client" << std::endl;
			if (!client_tcp()) std::cerr << "Error opening client\n";
		}
		else {
			std::cerr << "UDP Client" << std::endl;
			if (!client_udp()) std::cerr << "Error opening client\n";
		}
		// Resize the local buffer
		if (inputSignals[0]->getValueType()==(signal_value_type::t_message))
			ip_tunnel_buffer.resize(inputSignals[0]->getBufferLength()*MAX_MESSAGE_SIZE);
		else
			ip_tunnel_buffer.resize(inputSignals[0]->getBufferLength()*inputSignals[0]->valueTypeSizeOf() / sizeof(char));	
	}
}


bool IPTunnel::runBlock(void)
{
	size_t process;

	// Run Server/Receiver
	if (inputSignals.empty())
	{ 

		size_t space = outputSignals[0]->space();
		for (auto k : outputSignals)
			space = min(space, k->space());
		
		if (space == 0) return false;

		//----------------------------------------RECEIVING THE SIGNAL----------------------------------------

		if (transportProtocol == transport_protocol::TCP)
		{
			process = ipTunnelRecv_tcp(space);
		}
		else 
		{
			process = ipTunnelRecv_udp(space);
		}
		
		if (process == 0) return false;

		if (getLogValue()) std::cerr << "Samples received through IP Tunnel: " << process << std::endl;
	
		if (outputSignals[0]->getValueType() == (signal_value_type::t_message))
		{
			size_t aux{ 0 };
			while (aux < process)
			{
				t_message msg;
				msg.messageRaw.resize(MESSAGE_TYPE_SIZE + MESSAGE_DATA_LENGTH_SIZE);

				for (auto k = 0; k < MESSAGE_TYPE_SIZE; k++)
					msg.messageRaw[k] = ip_tunnel_buffer[aux+k];
				for (auto k = 0; k < MESSAGE_DATA_LENGTH_SIZE; k++)
					msg.messageRaw[MESSAGE_TYPE_SIZE+k] = ip_tunnel_buffer[aux+MESSAGE_TYPE_SIZE + k];
				size_t dataLength = strToNumber(msg.getMessageDataLength());
				msg.messageRaw.resize(MESSAGE_TYPE_SIZE + MESSAGE_DATA_LENGTH_SIZE+dataLength);
				for (auto k = 0; k < dataLength; k++)
					msg.messageRaw[MESSAGE_TYPE_SIZE + MESSAGE_DATA_LENGTH_SIZE + k] = ip_tunnel_buffer[aux+MESSAGE_TYPE_SIZE + MESSAGE_DATA_LENGTH_SIZE + k];

				for (auto k : outputSignals)
				{
					k->bufferPut(msg);
				}

				aux = aux + msg.size();
			}
		}
		else
		{
			size_t aux = (process * sizeof(char) / outputSignals[0]->valueTypeSizeOf());
			for (auto k : outputSignals) 
			{
				k->bufferPut((std::byte*) &(ip_tunnel_buffer[0]), aux);
			}
		}
	}
	// Run Client/Transmitter
	else {
		
		size_t ready = inputSignals[0]->ready();

		if (ready == 0) return false;

		if (inputSignals[0]->getValueType() == (signal_value_type::t_message))
		{
			ip_tunnel_buffer.resize(0);
			t_message msg;
			for (auto k = 0; k < ready; k++)
			{
				inputSignals[0]->bufferGet((t_message *) &msg);
				std::copy(msg.messageRaw.begin(), msg.messageRaw.end(), std::back_inserter(ip_tunnel_buffer));
			}
			process = ip_tunnel_buffer.size();
		}
		else
		{
			inputSignals[0]->bufferGet((std::byte*) &(ip_tunnel_buffer[0]), ready);
			process = ready * inputSignals[0]->valueTypeSizeOf() / sizeof(char);
		}

		if (transportProtocol == transport_protocol::TCP) {
			process=ipTunnelSend_tcp(process);
		}
		else {
			process = ipTunnelSend_udp(process);
		};

		if (process == 0) return false;

		if (getLogValue()) std::cerr << "Samples send through IP Tunnel: " << process << std::endl;
	}

	return true;
}


void IPTunnel::terminate(void) {
	for (auto k = clientSocket.begin(); k!=clientSocket.end(); k++)	closesocket(*k);
	for (auto k = serverSocket.begin(); k != serverSocket.end(); k++) closesocket(*k);
	WSACleanup();
}

size_t IPTunnel::ipTunnelSend_tcp(size_t process) {
	size_t remaining = process;
	size_t result{ 0 };
	size_t sent{ 0 };
	while (remaining > 0) {
		result = send(clientSocket[0], &(ip_tunnel_buffer[0]) + sent, (int) remaining, 0);
		if (result >= 0) {
			remaining -= result;
			sent += result;
		}
		else { 
			if (getLogValue()) {
				if ( result == 0) std::cerr << "No data send through TCP" << std::endl;
				else std::cerr << "ERROR sending TCP, error #: " << WSAGetLastError() << std::endl;
			}
			break;
		}
	}
	return sent;
}

size_t IPTunnel::ipTunnelSend_udp(size_t process) {

	sockaddr_in hint;
	hint.sin_family = AF_INET; 
	inet_pton(AF_INET, remoteMachineIpAddress.data(), &hint.sin_addr.S_un.S_addr);
	hint.sin_port = ntohs((u_short)remoteMachinePort);

	size_t remaining = process;
	size_t result{ 0 };
	size_t sent{ 0 };
	while (remaining > 0) {
		result = sendto(clientSocket[0], &(ip_tunnel_buffer[0]) + sent, (int)remaining, 0, (sockaddr*) &hint, INET_ADDRSTRLEN);
		if (result > 0) {
			remaining -= result;
			sent += result;
		}
		else {
			if (getLogValue()) {
				if (result == 0) std::cerr << "No data send through UDP" << std::endl;
				else std::cerr << "ERROR sending UDP, error #: " << WSAGetLastError() << std::endl;
			}
			break;
		}
	}
	return sent;
}


size_t IPTunnel::ipTunnelRecv_tcp(size_t space) {
	char* recv_buffer = &ip_tunnel_buffer[0];
	size_t remaining{ 0 };
	if (outputSignals[0]->getValueType() == (signal_value_type::t_message))
	{
		remaining = ip_tunnel_buffer.size();
	}
	else
	{
		remaining = space * outputSignals[0]->valueTypeSizeOf();
	}
	size_t received{ 0 };
	while (remaining > 0) {
		auto aux{ 0 };
		aux = recv(serverSocket[1], recv_buffer + received, (int) remaining, 0);
		if (aux > 0) {
			received += aux;
			remaining -= received;
		}
		else {
			if (getLogValue()) {
				if (aux == 0) std::cerr << "No data received through TCP" << std::endl;
				else std::cerr << "ERROR receiving TCP, error #: " << WSAGetLastError() << std::endl;
			}
			break;
		}
	}
	return received;
}

size_t IPTunnel::ipTunnelRecv_udp(size_t space) {

	sockaddr_in senderAddress;
	int senderAddressSize = sizeof(senderAddress);

	char* recv_buffer = &ip_tunnel_buffer[0];
	size_t remaining = space * outputSignals[0]->valueTypeSizeOf();
	size_t received{ 0 };
	while (remaining > 0) {
		auto aux{ 0 };
		aux = recvfrom(serverSocket[0], recv_buffer + received, (int) remaining, 0, (sockaddr *) &senderAddress, &senderAddressSize);
		if (aux > 0) {
			received += aux;
			remaining -= received;
		}
		else {
			if (getLogValue()) {
				if (aux == 0) std::cerr << "No data received through UDP" << std::endl;
				else std::cerr << "ERROR receiving UDP, error #: " << WSAGetLastError() << std::endl;
			}
			break;
		}
	}
	return received;
}

bool IPTunnel::server_tcp() {

// Steps to create the TCP Server			// https://docs.microsoft.com/en-us/windows/win32/winsock
// 1 - Initialize Winsock				
// 2 - Create a socket
// 3 - Bind the socket
// 4 - Listen on the socket for a client
// 5 - Accept a connection from a client

	// STEP 1 - Initialize Winsock

	WSADATA wsData;								// The WSADATA structure can store information about the Windows Sockets implementation.
	WORD ver = MAKEWORD(2, 2);					// The MAKEWORD(2,2) parameter of WSAStartup makes a request for version 2.2 of Winsock on the system
	int wsResult = WSAStartup(ver, &wsData);	// The WSAStartup function initiates use of the Winsock DLL by a process.
	if (wsResult != 0)							// If successful, the WSAStartup function returns zero. Otherwise, it returns a error code.
	{
		std::cerr << "Can't start Winsock. Error code #" << wsResult << std::endl;
		return false;
	}

	// STEP 2 - Create a socket					

	// The socket function creates a socket that is bound to a specific transport service provider.
	// If no error occurs, socket returns a descriptor referencing the new socket.
	// Otherwise, a value of INVALID_SOCKET is returned, and a specific error code can be retrieved by calling WSAGetLastError.
	//
	//	serverSocket = socket(AF_INET, SOCK_STREAM, 0);
	//	serverSocket: socket descriptor, an integer(like a file - handle)
	//	domain : integer, communication domain e.g., AF_INET(IPv4 protocol), AF_INET6(IPv6 protocol)
	//	type : communication type
	//		SOCK_STREAM : TCP(reliable, connection oriented)
	//		SOCK_DGRAM : UDP(unreliable, connectionless), SOCK_DGRAM = 2
	//	protocol : Protocol value for Internet Protocol(IP), which is 0. 
	//	This is the same number which appears on protocol field in the IP header of a packet.

	SOCKET s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	serverSocket.push_back(s);
	
	if (serverSocket[0] == INVALID_SOCKET)
	{
		std::cerr << "Can't create socket, Error Code #" << WSAGetLastError() << std::endl;
		WSACleanup();
		return false;
	}

	// STEP 3 - Bind the socket
	// For a server to accept client connections, it must be bound to a network address and port.
	// After creation of the socket, bind function binds the socket to the address
	// and port number specified in hint(custom data structure).
	// We bind the server to the localhost, hence we use INADDR_ANY 
	// to specify the IP address

	sockaddr_in hint;
	hint.sin_family = AF_INET; // AF_INET=2, IPv4
	inet_pton(AF_INET, localMachineIpAddress.data(), &hint.sin_addr.S_un.S_addr);
	hint.sin_port = ntohs((u_short)localMachinePort);	// The htons() function converts the unsigned short integer hostshort from host byte order to network byte order. On the i386 the host byte order is Least Significant Byte first, whereas the network byte order, as used on the Internet, is Most Significant Byte first.
	
	char ipAddress[INET_ADDRSTRLEN];
	inet_ntop(AF_INET, &hint.sin_addr.S_un.S_addr, ipAddress, INET_ADDRSTRLEN);
	std::cerr << "The TCP server is running on IP address: " << ipAddress;
	std::cerr << " Port: " << htons(hint.sin_port) << std::endl;

	if (bind(serverSocket[0], (sockaddr*)& hint, sizeof(hint)) < 0) {
		std::cerr << "Bind failed with error code #" << WSAGetLastError() << std::endl;
		return false;
	}

	// STEP 4 - Listen on the socket for a client
	// It puts the server socket in a passive mode, where it waits for the client
	// to approach the server to make a connection.
	// The backlog, defines the maximum length to which the queue of pending connections may grow.
	// If a connection request arrives when the queue is full, the client may receive an error with
	// an indication of ECONNREFUSED.

	if (listen(serverSocket[0], SOMAXCONN) == -1) {
			std::cerr << "Listen error!" << std::endl;
			return false;
	}

	// STEP 5 - Accept a connection from a client
	// It extracts the first connection request on the queue of pending
	// connections for the listening socket, creates a new connected socket,
	// and returns a new socket descriptor referring to that socket.
	// At this point, connection is established between client and server,
	// and they are ready to transfer data

	sockaddr_in client;
	int clientSize = sizeof(client);

	s = accept(serverSocket[0], (sockaddr*) &client, &clientSize);
	serverSocket.push_back(s);

	// Provides information
	char host[NI_MAXHOST];
	char service[NI_MAXSERV];

	ZeroMemory(host, NI_MAXHOST);
	ZeroMemory(service, NI_MAXSERV);

	if (getnameinfo((sockaddr*)& client, sizeof(client), host, NI_MAXHOST, service, NI_MAXSERV, 0) == 0)
	{
		std::cerr << host << " connected on port " << service << std::endl;
	}
	else
	{
		inet_ntop(AF_INET, &client.sin_addr, host, NI_MAXHOST);
		std::cerr << host << " connected on port " << ntohs(client.sin_port) << std::endl;
	}
	
	return true;
}

bool IPTunnel::server_udp() {

	// Steps to create the UDP Server			// https://docs.microsoft.com/en-us/windows/win32/winsock
	// 1 - Initialize Winsock				
	// 2 - Create a socket
	// 3 - Bind the socket

	
	// STEP 1 - Initialize Winsock
	WSADATA wsData;								// The WSADATA structure can store information about the Windows Sockets implementation.
	WORD ver = MAKEWORD(2, 2);					// The MAKEWORD(2,2) parameter of WSAStartup makes a request for version 2.2 of Winsock on the system
	int wsResult = WSAStartup(ver, &wsData);	// The WSAStartup function initiates use of the Winsock DLL by a process.
	if (wsResult != 0)							// If successful, the WSAStartup function returns zero. Otherwise, it returns a error code.
	{
		std::cerr << "Can't start Winsock. Error code #" << wsResult << std::endl;
		return false;
	}

	// STEP 2 - Create a socket					

	// The socket function creates a socket that is bound to a specific transport service provider.
	// If no error occurs, socket returns a descriptor referencing the new socket.
	// Otherwise, a value of INVALID_SOCKET is returned, and a specific error code can be retrieved by calling WSAGetLastError.
	//
	//	serverSocket = socket(AF_INET, SOCK_STREAM, 0);
	//	serverSocket: socket descriptor, an integer(like a file - handle)
	//	domain : integer, communication domain e.g., AF_INET(IPv4 protocol), AF_INET6(IPv6 protocol)
	//	type : communication type
	//		SOCK_STREAM : TCP(reliable, connection oriented)
	//		SOCK_DGRAM : UDP(unreliable, connectionless), SOCK_DGRAM = 2
	//	protocol : Protocol value for Internet Protocol(IP), which is 0. 
	//	This is the same number which appears on protocol field in the IP header of a packet.

	SOCKET s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	serverSocket.push_back(s);

	if (serverSocket[0] == INVALID_SOCKET)
	{
		std::cerr << "Can't create socket, Error Code #" << WSAGetLastError() << std::endl;
		WSACleanup();
		return false;
	}

	// STEP 3 - Bind the socket
	// For a server to accept client connections, it must be bound to a network address and port.
	// After creation of the socket, bind function binds the socket to the address
	// and port number specified in hint(custom data structure).
	// We bind the server to the localhost, hence we use INADDR_ANY 
	// to specify the IP address

	sockaddr_in hint;
	hint.sin_family = AF_INET; // AF_INET=2, IPv4
	inet_pton(AF_INET, localMachineIpAddress.data(), &hint.sin_addr.S_un.S_addr);
	hint.sin_port = ntohs((u_short)localMachinePort);	// The htons() function converts the unsigned short integer hostshort from host byte order to network byte order. On the i386 the host byte order is Least Significant Byte first, whereas the network byte order, as used on the Internet, is Most Significant Byte first.

	char ipAddress[INET_ADDRSTRLEN];
	inet_ntop(AF_INET, &hint.sin_addr.S_un.S_addr, ipAddress, INET_ADDRSTRLEN);
	std::cerr << "The UDP server is running on IP address: " << ipAddress;
	std::cerr << " Port: " << htons(hint.sin_port) << std::endl;

	if (bind(serverSocket[0], (sockaddr*)& hint, sizeof(hint)) == SOCKET_ERROR) {
		std::cerr << "Bind failed with error code #" << WSAGetLastError() << std::endl;
		return false;
	}
	
	return true;
}

bool IPTunnel::client_tcp() {

	// Steps to create the Client				// https://docs.microsoft.com/en-us/windows/win32/winsock
	// 1 - Initialize Winsock
	// 2 - Create a socket
	// 3 - Connect to the server

	// STEP 1 - Initialize Winsock
	WSADATA wsData;								// The WSADATA structure can store information about the Windows Sockets implementation.
	WORD ver = MAKEWORD(2, 2);					// The MAKEWORD(2,2) parameter of WSAStartup makes a request for version 2.2 of Winsock on the system
	int wsResult = WSAStartup(ver, &wsData);	// The WSAStartup function initiates use of the Winsock DLL by a process.
	if (wsResult != 0)							// If successful, the WSAStartup function returns zero. Otherwise, it returns a error code.
	{
		std::cerr << "Can't start Winsock. Error code #" << wsResult << std::endl;
		return false;
	}

	// STEP 2 - Create a socket					// After initialization, a SOCKET object must be instantiated for use by the client

	// The socket function creates a socket that is bound to a specific transport service provider.
	// If no error occurs, socket returns a descriptor referencing the new socket.
	// Otherwise, a value of INVALID_SOCKET is returned, and a specific error code can be retrieved by calling WSAGetLastError.
	//
	//	clientSocket = socket(AF_INET, SOCK_STREAM, 0);
	//	clientSockey: socket descriptor, an integer(like a file - handle)
	//	domain : integer, communication domain e.g., AF_INET(IPv4 protocol), AF_INET6(IPv6 protocol)
	//	type : communication type
	//		SOCK_STREAM : TCP(reliable, connection oriented)
	//		SOCK_DGRAM : UDP(unreliable, connectionless)
	//	protocol : Protocol value for Internet Protocol(IP), which is 0. 
	//	This is the same number which appears on protocol field in the IP header of a packet.
	
	SOCKET s = socket(AF_INET, SOCK_STREAM, 0);
	clientSocket.push_back(s);
		
	if (clientSocket[0] == INVALID_SOCKET)
	{
		std::cerr << "Can't create socket, Error Code #" << WSAGetLastError() << std::endl;
		WSACleanup();
		return false;
	}

	// STEP 3 - Connect to the server

	sockaddr_in hint;
	hint.sin_family = AF_INET;
	hint.sin_port = htons((u_short)localMachinePort);
	inet_pton(AF_INET, remoteMachineIpAddress.c_str(), &hint.sin_addr);

	int connResult{ -2 };
	while (connResult != 0 || numberOfTrials == 0) {
		connResult = connect(clientSocket[0], (sockaddr*)& hint, sizeof(hint));
		if (connResult == SOCKET_ERROR)
		{
			std::cerr << "Can't connect to server, Err #" << WSAGetLastError() << std::endl;
			std::cerr << "Waiting " << timeIntervalSeconds << " seconds." << std::endl;
			Sleep(timeIntervalSeconds * 1000);
		}

		if (--numberOfTrials == 0) {
			std::cerr << "Reached maximum number of attempts." << std::endl;
		}
	}
	std::cerr << "Connected!\n";
	return true;
}

bool IPTunnel::client_udp() {

	// Steps to create the Client				// https://docs.microsoft.com/en-us/windows/win32/winsock
	// 1 - Initialize Winsock
	// 2 - Create a socket
	// 3 - Bind the socket
	
	// STEP 1 - Initialize Winsock
	WSADATA wsData;								// The WSADATA structure can store information about the Windows Sockets implementation.
	WORD ver = MAKEWORD(2, 2);					// The MAKEWORD(2,2) parameter of WSAStartup makes a request for version 2.2 of Winsock on the system
	int wsResult = WSAStartup(ver, &wsData);	// The WSAStartup function initiates use of the Winsock DLL by a process.
	if (wsResult != 0)							// If successful, the WSAStartup function returns zero. Otherwise, it returns a error code.
	{
		std::cerr << "Can't start Winsock. Error code #" << wsResult << std::endl;
		return false;
	}

	// STEP 2 - Create a socket					// After initialization, a SOCKET object must be instantiated for use by the client

	// The socket function creates a socket that is bound to a specific transport service provider.
	// If no error occurs, socket returns a descriptor referencing the new socket.
	// Otherwise, a value of INVALID_SOCKET is returned, and a specific error code can be retrieved by calling WSAGetLastError.
	//
	//	clientSocket = socket(AF_INET, SOCK_STREAM, 0);
	//	clientSockey: socket descriptor, an integer(like a file - handle)
	//	domain : integer, communication domain e.g., AF_INET(IPv4 protocol), AF_INET6(IPv6 protocol)
	//	type : communication type
	//		SOCK_STREAM : TCP(reliable, connection oriented)
	//		SOCK_DGRAM : UDP(unreliable, connectionless)
	//	protocol : Protocol value for Internet Protocol(IP), which is 0. 
	//	This is the same number which appears on protocol field in the IP header of a packet.

	SOCKET s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	clientSocket.push_back(s);

	if (clientSocket[0] == INVALID_SOCKET)
	{
		std::cerr << "Can't create socket, Error Code #" << WSAGetLastError() << std::endl;
		WSACleanup();
		return false;
	}

	std::cerr << "The UDP client socket was created!" << std::endl;

	return true;
}

