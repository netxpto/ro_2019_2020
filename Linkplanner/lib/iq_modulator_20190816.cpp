# include "iq_modulator_20190816.h"

void IQModulator::initialize(void) 
{

	firstTime = false;

	outputSignals[0]->setSymbolPeriod(inputSignals[0]->getSymbolPeriod());
	outputSignals[0]->setSamplingPeriod(inputSignals[0]->getSamplingPeriod());
	outputSignals[0]->setFirstValueToBeSaved(inputSignals[0]->getFirstValueToBeSaved());

	outputSignals[0]->setCentralWavelength(outputOpticalWavelength);
	outputSignals[0]->setCentralFrequency(outputOpticalFrequency);

}

bool IQModulator::runBlock(void) 
{

	int ready0 = inputSignals[0]->ready();
	int ready1 = inputSignals[1]->ready();
	int ready = std::min(ready0, ready1);

	int space = outputSignals[0]->space();
	   
	int process = std::min(ready, space);

	if (process == 0) return false;

	t_real re, im;
	for (int i = 0; i < process; i++) {

		inputSignals[0]->bufferGet(&re);
		inputSignals[1]->bufferGet(&im);

		t_complex valueX(re, im);
		valueX = 0.5*sqrt(outputOpticalPower)*valueX;

		signal_value_type sType = outputSignals[0]->getValueType();

		switch (sType) {
			case signal_value_type::t_complex:
				outputSignals[0]->bufferPut(valueX);
				break;
			case signal_value_type::t_complex_xy:
				t_complex valueY(0, 0);
				t_complex_xy valueXY = {valueX, valueY};
				outputSignals[0]->bufferPut(valueXY);
				break;
		}
	}
	return true;

}


