# include "linear_equalizer_20200212.h"

void LinearEqualizer::initialize(void) {

	outputSignals[0]->setSymbolPeriod(inputSignals[0]->getSymbolPeriod());
	outputSignals[0]->setSamplingPeriod(inputSignals[0]->getSamplingPeriod());
	//outputSignals[0]->setFirstValueToBeSaved(inputSignals[0]->getFirstValueToBeSaved()); 

	/* preliminary calculations for parameter conversion */
	t_real L = fiberLength * 1e3;								 // Fiber length (km --> m) 
	t_real D = 1e-6 * dispersionCoeficient;						 // Group - velocity dispersion parameter (ps/km/nm --> s/m^2)
	t_real c = SPEED_OF_LIGHT;									 // speed of light (m/s)
	t_real lambda = centralWavelength * 1e-9;					 // central wavelength (nm --> m)
	t_real fc = c / lambda;										 // central frequency (Hz)
	int length = 2 * inputSignals[0]->getBufferLength();         // set the linear operator transfer dunction length (2*bufferLength)

	/* calculation of beta2 */
	t_real beta2 = -D * lambda * lambda / (2 * PI * c);           // calculate beta2 value

	/* angular frequency vector */
	t_real dt = inputSignals[0]->getSamplingPeriod();             // sampling period
	t_real fs = 1 / dt;											  // samplinf frequency	
	int    nt = length;											  // length of the signal block

	std::vector<int> itegerVec(nt);
	for (int i = 0; i < (nt / 2); i++)         // fill vector for the positive frequency (i.e. for nt = 1024, it fills (0:1:511))
		itegerVec[i] = i;
	for (int i = 0; i < nt / 2; i++)
		itegerVec[nt / 2 + i] = -nt / 2 + i;   // fill vector for the negative frequency (i.e. for nt = 1024, it fills (-512:1:-1))

	std::vector<t_complex> w(nt);              // angular frequency vector
	for (int i = 0; i < nt; i++)
		w[i] = 2 * PI * itegerVec[i] / (dt * (double)nt);

	std::vector<t_complex> operatorD(nt);      // cde operator
	for (int i = 0; i < nt; i++) {
		operatorD[i] = {0 ,-beta2 * pow(w[i].real(), 2) / 2.0 };                                     
	}

	/* calculate the cde tf  --> exp(-i*operator D * fiber_length) */
	std::vector<t_complex> exponentail_operatorD(nt);
	for (int i = 0; i < nt; i++)
		exponentail_operatorD[i] = exp(-operatorD[i] * L);    //  cd compensation tf --> exp(-i*operator D*fiber_length)

	cde = exponentail_operatorD;

/*	std::ofstream b;
	b.open("b.txt");
	b.precision(15);
	for (size_t i = 0; i < cde.size(); i++)
	{
		b << cde[i].imag() << std::endl;
	}
	b.close();

	std::ofstream a;
	a.open("a.txt");
	a.precision(15);
	for (int i = 0; i < cde.size(); i++)
	{
		a << cde[i].real() << std::endl;
	}
	a.close();
*/
}

bool LinearEqualizer::runBlock(void) {

	int ready = inputSignals[0]->ready();
	int space = outputSignals[0]->space();
	int process = std::min(ready, space);
	if (process == 0) return false;

	/* get the input signal for as a vector*/
	size_t block_size = cde.size();

	std::vector<t_complex> currentCopy(process);   // Get the Input signal
	t_complex input;
	for (int i = 0; i < process; i++) {
		inputSignals[0]->bufferGet(&input);
		currentCopy.at(i) = input;
	}

	if (K > 0)
	{
		//////////////////////// DATA BLOCK LENGTH ADJUSTMENT : START ////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////
		/* this part deals especially last transmitted signal which may or may not be same as bufferLength !
		Since our transfer function length is 2*bufferLength, it is necessary to make data length equal to
		2*bufferLength by concatenating the data. */
		if (previousCopy.size() != currentCopy.size()) {
			std::vector<t_complex> currentCopyAux;   // it'll concatenate the currentCopy integer number of time --> currentCopyAux (output)
			while (currentCopyAux.size() <= previousCopy.size()) {
				for (size_t i = 0; i < currentCopy.size(); i++) {
					currentCopyAux.push_back(currentCopy[i]);
				}
			}

			std::vector<t_complex> cc(previousCopy.size());  // it'll truncate the currentCopyAux to the previousCopy size --> cc (output)
			for (size_t i = 0; i < previousCopy.size(); i++) {
				cc[i] = currentCopyAux[i];
			}
			currentCopy = cc;
		}

		std::vector<t_complex> inputSignal;
		inputSignal.reserve(previousCopy.size() + currentCopy.size());
		inputSignal.insert(inputSignal.end(), previousCopy.begin(), previousCopy.end());
		inputSignal.insert(inputSignal.end(), currentCopy.begin(), currentCopy.end());

		//////////////////////// DATA BLOCK LENGTH ADJUSTMENT : END //////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////

		/* initial variables for SSFM */
		std::vector<t_complex> utime = inputSignal;	// reference copy of the input
		std::vector<t_complex> uCDE(block_size);    // CD compensated in freq domain
		std::vector<t_complex> uCde(block_size);    // CD compensated in time domain

		//////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////// CD COMPENSATION : START ///////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////
		std::vector<t_complex> ufft = ifft(utime); // fft of the input data
		for (size_t i = 0; i < block_size; i++)
			uCDE[i] = cde[i] * ufft[i];            // apply cd compensation
		uCde = fft(uCDE);                          // ifft
		//////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////// CD COMPENSATION : snd ////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////

		for (int i = 0; i < process; i++) {
			outputSignals[0]->bufferPut(uCde[block_size / 4 + i]);
		}
	}

	previousCopy = currentCopy;
	K = K + 1;
	L = L + 1;
	return true;
}

