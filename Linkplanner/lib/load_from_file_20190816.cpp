#include "load_from_file_20190816.h"

void LoadFromFile::initialize(void) 
{

	inFile.open(fileName, std::ios::in | std::ios::binary);

	if (!inFile) 
	{
		std::cerr << "ERROR: load_from_file.cpp (can't open input file!)" << "\n";
		system("pause");
		system("exit");
	}

	if (fileName.substr(fileName.find_last_of(".") + 1) == "sgn") processHeader();
	
	for (auto k : outputSignals)
	{
		k->setSymbolPeriod(symbolPeriod);
		k->setSamplingPeriod(samplingPeriod);
	}

}

void LoadFromFile::terminate(void)
{
 	inFile.close();
}

bool LoadFromFile::runBlock(void) {

	int space = outputSignals[0]->space();

	if (!space) return false;

	std::streampos size = space * outputSignals[0]->valueTypeSizeOf();

	std::streampos	begin = inFile.tellg();
	inFile.seekg(0, std::ios::end);
	std::streampos	end = inFile.tellg();
	inFile.seekg(begin, std::ios::beg);

	size = std::min((int)size, (int)(end - begin));

	if (size==0) return false;

	std::unique_ptr<char> memblock(new char[size]);
	inFile.read(memblock.get(), size);

	for (auto k : outputSignals)
	{
		k->bufferPut((std::byte*) memblock.get(), size / outputSignals[0]->valueTypeSizeOf());
	}

	return true;

}

void LoadFromFile::processHeader()
{
	std::string line;

	getline(inFile, line);

	while (line.compare("// ### HEADER TERMINATOR ###\r"))
	{

		if (std::size_t found = line.find("Signal type:")!= std::string::npos)
		{	
		}
		else if (std::size_t found = line.find("Symbol Period (s):") != std::string::npos)
		{
			line.replace(line.find("Symbol Period (s):"), 18, "");
			std::stringstream aux{ line };
			aux >> symbolPeriod;
		}
		else if (std::size_t found = line.find("Sampling Period (s): ") != std::string::npos)
		{
			line.replace(line.find("Sampling Period (s):"), 20, "");
			std::stringstream aux{ line };
			aux >> samplingPeriod;
		}

		getline(inFile, line);
	}

}
