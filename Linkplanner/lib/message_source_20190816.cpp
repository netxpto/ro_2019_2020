# include "message_source_20190816.h"

bool MessageSource::runBlock(void) {

	int space{ MAX_BUFFER_LENGTH };
	for (auto k : outputSignals)
		space=std::min(space,k->space());

	if ( (numberOfMessages==0) || (space == 0) ) return false;

	long int process{ 0 };
	if (numberOfMessages > 0) process = std::min(numberOfMessages, (long int) space);

	for (auto k = 0; k < process; k++) {
		if (getFirstRun() || (mode == MessageSourceMode::OneByOne)) {
			std::string str;
			std::cout << "Enter message type: ";
			std::cin >> str;
			message.setMessageType(str);
			std::cout << "Enter message length: ";
			std::cin >> str;
			message.setMessageDataLength(str);
			std::cout << "Enter message data: ";
			std::cin >> str;
			message.setMessageData(str);

			setFirstRun(false);
		}

		for (auto k : outputSignals)
			k->bufferPut(message);

		numberOfMessages--;
	}
	
	return true;

}
	

