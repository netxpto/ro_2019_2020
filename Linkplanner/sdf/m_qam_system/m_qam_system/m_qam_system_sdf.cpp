# include "m_qam_system_sdf.h"

using namespace std; //to be removed

// Compiling ########################################################
// 
// Visual Studio 2019 (v142)
// ISO C++17 Standard
// Windows SDK 10.0.18362.0
//
// ##################################################################

int main(int argc, char* argv[])
{
	// Input Parameters Declaration #################################
	
	MQamSystemInputParameters param = MQamSystemInputParameters(argc, argv);

	// Signals Declaration #########################################

	OpticalSignal TxLocalOscillator_Out{ "TxLocalOscillator_Out.sgn"};
	Binary BinarySource_Out_0{ "BinarySource_Out_0.sgn" };
	Binary BinarySource_Out_1{ "BinarySource_Out_1.sgn" };
	OpticalSignal MQAMTransmitter_Out{ "MQAMTransmitter_Out.sgn" };
	/*
	OpticalSignal BoosterAmplifier_Out{ "BoosterAmplifier_Out.sgn" };
	OpticalSignal Fiber_Out{ "Fiber_Out.sgn" };
	OpticalSignal PreAmplifier_Out{ "PreAmplifier_Out.sgn" };
	OpticalSignal OpticalFilter_Out{ "OpticalFilter_Out.sgn" };
	*/
	OpticalSignal RxLocalOscillator_Out{ "RxLocalOscillator_Out.sgn" };
	Binary MQAMReceiver_Out{ "MQAMReceiver_Out.sgn" };
	Binary BitErrorRate_Out{ "BitErrorRate_Out_.sgn" };

	// #############################################################

	// Blocks Declaration ##########################################

	BinarySource BinarySource_{ {}, {&BinarySource_Out_0, &BinarySource_Out_1} };
	BinarySource_.setBitPeriod(param.symbolPeriod_s / log2(param.constellationCardinality));
	BinarySource_.setNumberOfBits(param.numberOfBits);

	// -------------------------------------------------------------

	LocalOscillator TxLocalOscillator_{ {}, {&TxLocalOscillator_Out} };
	TxLocalOscillator_.setSymbolPeriod(param.symbolPeriod_s);
	TxLocalOscillator_.setSamplingPeriod(param.symbolPeriod_s / param.samplesPerSymbol);
	TxLocalOscillator_.setOpticalPower_dBm(param.txLocalOscillatorPower_dBm);
	
	// -------------------------------------------------------------

	MQAMTransmitter MQAMTransmitter_{ {&BinarySource_Out_1, &TxLocalOscillator_Out },{&MQAMTransmitter_Out} };
	MQAMTransmitter_.setSuperBlockFolderName("signals/SuperBlock_MQAMTransmitter");
	MQAMTransmitter_.setSaveInternalSignals(param.MQAMTransmitterSaveInternalSignals);
	MQAMTransmitter_.setLogFileName("log_MQAMTransmitter.txt");
	MQAMTransmitter_.setLogValue(param.MQAMTransmitterLogValue);
	MQAMTransmitter_.setNumberOfSamplesPerSymbol(param.samplesPerSymbol);
	MQAMTransmitter_.setImpulseResponseTimeLength_symbolPeriods(param.txPulseShaperLength_symbolPeriods);
	MQAMTransmitter_.setPulseShaperFilterType(param.txPulseShaperType);
	MQAMTransmitter_.setRollOffFactor(param.raisedCosineRollOffFactor);
	MQAMTransmitter_.setBTs(param.gaussianBandwidthSymbolPeriodProduct);
	MQAMTransmitter_.setSavePulseShapersImpulseResponse(param.MQAMTransmitterSavePulseShapersImpulseResponse);
	MQAMTransmitter_.setRemovePulseShaperFilterDelay(param.removeTransmitterPulseShaperFilterDelay);

	// -------------------------------------------------------------
	/*
	EDFA BoosterAmplifier_{ {&MQAMTransmitter_Out},{&BoosterAmplifier_Out} };
	BoosterAmplifier_.setSuperBlockFolderName("signals/SuperBlock_BoosterAmplifier");
	BoosterAmplifier_.setSaveInternalSignals(param.boosterAmplifierSaveInternalSignals);
	BoosterAmplifier_.setLogFileName("log_BoosterAmplifier.txt");
	BoosterAmplifier_.setLogValue(true);
	BoosterAmplifier_.setGain_dB(param.boosterAmplifierGain_dB);
	BoosterAmplifier_.setNoiseFigure_dB(param.boosterAmplifierNoiseFigure_dB);

	// -------------------------------------------------------------

	Fiber Fiber_{ {&BoosterAmplifier_Out},{&Fiber_Out} };
	Fiber_.setAttenuationCoeficient(param.fiberAttenuation_dB);
	Fiber_.setDispersionCoeficient(param.dispersionCoeficient);
	Fiber_.setFiberLength(param.fiberLength_km);
	Fiber_.setStepLength(param.stepLength_m);
	Fiber_.setNlCoeff(param.nlCoeff);

	// -------------------------------------------------------------

	EDFA PreAmplifier_{ {&Fiber_Out}, {&PreAmplifier_Out} };
	PreAmplifier_.setSuperBlockFolderName("signals/SuperBlock_PreAmplifier");
	PreAmplifier_.setSaveInternalSignals(param.preAmplifierSaveInternalSignals);
	PreAmplifier_.setLogFileName("log_PreAmplifier.txt");
	PreAmplifier_.setLogValue(true);
	PreAmplifier_.setGain_dB(param.preAmplifierGain_dB);
	PreAmplifier_.setNoiseFigure_dB(param.preAmplifierNoiseFigure_dB);

	// -------------------------------------------------------------

	OpticalFilter OpticalFilter_{ {&PreAmplifier_Out},{&OpticalFilter_Out} };
	OpticalFilter_.setBandwidth_Hz(100000e9);
	OpticalFilter_.setFilterType(electrical_filter_type::LowPass);
	OpticalFilter_.setImpulseResponseTimeLength(param.txPulseShaperLength_symbolPeriods);
	OpticalFilter_.setSaveInternalSignals(false);
	*/
	// -------------------------------------------------------------

	LocalOscillator RxLocalOscillator_{ {}, {&RxLocalOscillator_Out} };
	RxLocalOscillator_.setSymbolPeriod(param.symbolPeriod_s);
	RxLocalOscillator_.setSamplingPeriod(param.symbolPeriod_s / param.samplesPerSymbol);
	RxLocalOscillator_.setOpticalPower_dBm(param.rxLocalOscillatorPower_dBm);

	// -------------------------------------------------------------

	MQAMReceiver MQAMReceiver_{ {&MQAMTransmitter_Out, &TxLocalOscillator_Out },{&MQAMReceiver_Out} };
	MQAMReceiver_.setSuperBlockFolderName("signals/SuperBlock_MQAMReceiver");
	MQAMReceiver_.setSaveInternalSignals(param.MQAMReceiverSaveInternalSignals);
	MQAMReceiver_.setLogFileName("log_MQAMReceiver.txt");
	MQAMReceiver_.setLogValue(true);
	MQAMReceiver_.setTiAmplifierGain(param.rxTiAmplifierGain);
	MQAMReceiver_.setTiAmplifierImpulseResponseTimeLength(param.rxTiAmplifierImpulseResponseTimeLength_symbolPeriods);
	MQAMReceiver_.setTiAmplifierElectricalSeeBeginningOfImpulseResponse(false);
	MQAMReceiver_.setTiAmplifierRollOffFactor(param.raisedCosineRollOffFactor);
	MQAMReceiver_.setTiAmplifierPassiveFilterMode(true);
	MQAMReceiver_.setTiAmplifierSeeBeginningOfImpulseResponse(true);
	//MQAMReceiver_.setLinearEqualizerFiberLength(param.fiberLength_km);
	//MQAMReceiver_.setLinearEqualizerDispersionCoeficient(param.dispersionCoeficient);
	MQAMReceiver_.setMatchedFilterImpulseResponseType(param.rxReceiverElectricalFilterImpulseResponseType);
	MQAMReceiver_.setMatchedFilterImpulseResponseTimeLength(param.txPulseShaperLength_symbolPeriods);
	MQAMReceiver_.setMatchedFilterRollOffFactor(param.raisedCosineRollOffFactor);
	MQAMReceiver_.setMatchedFilterImpulseResponseTimeLength(param.rxTiAmplifierImpulseResponseTimeLength_symbolPeriods);
	MQAMReceiver_.setMatchedFilterSeeBeginningOfImpulseResponse(true);
	MQAMReceiver_.setMatchedFilterPassiveFilterMode(true);
	MQAMReceiver_.setSamplesToSkip(param.rxSamplerSamplesToSkip);

	// -------------------------------------------------------------

	BitErrorRate BitErrorRate_{ {&BinarySource_Out_0, &MQAMReceiver_Out}, {&BitErrorRate_Out} };

	// -------------------------------------------------------------

	Sink Sink_{ {&BitErrorRate_Out} , {} };
	Sink_.setDisplayNumberOfSamples(true);
	Sink_.setNumberOfSamplesToProcess(param.numberOfSamplesToProcess);

	// ##############################################################

	// System Declaration ###########################################

	System MainSystem
	{ 
		{
			&BinarySource_,
			&TxLocalOscillator_,
			&MQAMTransmitter_,
			/*
			&BoosterAmplifier_,
			&Fiber_,
			&PreAmplifier_,
			&OpticalFilter_,
			*/
			&RxLocalOscillator_,
			&MQAMReceiver_,
			&BitErrorRate_,
			&Sink_
		},
		param.getOutputFolderName(),
		param.getLoadedInputParameters()
	};
	

	// ##############################################################

	// System Run ###################################################

	MainSystem.run();

	// ##############################################################

	// System Termination ###########################################
	
	MainSystem.terminate();

	// ##############################################################

	return 0;
}