# ifndef M_QAM_SYSTEM_H_
# define M_QAM_SYSTEM_H_

# include "netxpto_20190816.h"

# include "local_oscillator_20190816.h"
# include "binary_source_20190816.h"
# include "m_qam_transmitter_20190816.h"
# include "edfa_20190816.h"
# include "fiber_20190816.h"
# include "optical_filter_20190815.h"
# include "m_qam_receiver_20190816.h"
# include "bit_error_rate_20190816.h"
# include "sink_20190816.h"

class MQamSystemInputParameters : public SystemInputParameters {
public:

	// System Input Parameters #########################################
	string defaultInputFname{ "input_parameters_0.txt" };

	// Following are the transmitter parameters
	int constellationCardinality{ 4 };
	double symbolPeriod_s{ 2e-11 };
	int numberOfBits{ 1500 };
	int samplesPerSymbol{ 8 };
	string txPulseShaperType{ "RaisedCosine" };
	double raisedCosineRollOffFactor{ 0.1 };
	double gaussianBandwidthSymbolPeriodProduct{ 0.1 };
	int txPulseShaperLength_symbolPeriods{ 20 };
	double txLocalOscillatorPower_dBm{ 0 };
	bool MQAMTransmitterSaveInternalSignals{ false };
	bool MQAMTransmitterLogValue{ false };
	bool MQAMTransmitterSavePulseShapersImpulseResponse{ false };
	double boosterAmplifierGain_dB{ 10 };
	double boosterAmplifierNoiseFigure_dB{ 4 };
	bool boosterAmplifierSaveInternalSignals{ false };
	bool removeTransmitterPulseShaperFilterDelay{ false };


	// Following are the channel (optical fiber)
	double fiberAttenuation_dB = 0.2;
	double dispersionCoeficient = 16.0;
	double nlCoeff = 0.0013;
	double fiberLength_km = 50;
	double stepLength_m = 100;

	// Following are the receiver parameters
	double preAmplifierGain_dB{ 10 };
	double preAmplifierNoiseFigure_dB{ 4 };
	double opticalFilterBandwidth{ 50e9 };
	bool preAmplifierSaveInternalSignals{ false };
	electrical_filter_type rxOpticalFilterType{ electrical_filter_type::LowPass };
	double rxLocalOscillatorPower_dBm{ 6 };
	double rxTiAmplifierInputNoisePowerSpectralDensity{ 0 };
	int rxTiAmplifierImpulseResponseTimeLength_symbolPeriods{ txPulseShaperLength_symbolPeriods };
	//double rxTiAmplifierBandwidth{ 50.0e9 };
	double rxTiAmplifierGain{ 1 };
	double rxThermalNoisePower{ 0 };
	string rxReceiverElectricalFilterImpulseResponseType{ "NoFilter" };
	int rxSamplerSamplesToSkip{ (txPulseShaperLength_symbolPeriods - 1) * samplesPerSymbol + (rxTiAmplifierImpulseResponseTimeLength_symbolPeriods * samplesPerSymbol / 2) };
	int numberOfSamplesToProcess{ 1000 };
	bool MQAMReceiverSaveInternalSignals{ false };


	// ##################################################################


	// Initializes default input parameters
	MQamSystemInputParameters() : SystemInputParameters()
	{
		initializeInputParameterMap();
	}

	// Initializes input parameters according to the program arguments
	// Usage: .\m_qam_system_sdf.exe <input_parameters_0.txt> <output_directory>
	MQamSystemInputParameters(int argc, char* argv[]) : SystemInputParameters(argc, argv) {
		setInputParametersFileName(defaultInputFname);
		initializeInputParameterMap();
		readSystemInputParameters();
	}

	// Each parameter must be added to the parameter map by calling addInputParameter(string,param*)
	void initializeInputParameterMap() {
		addInputParameter("constellationCardinality", &constellationCardinality);
		addInputParameter("symbolPeriod_s", &symbolPeriod_s);
		addInputParameter("numberOfBits", &numberOfBits);
		addInputParameter("samplesPerSymbol", &samplesPerSymbol);
		addInputParameter("txPulseShaperType", &txPulseShaperType);
		addInputParameter("raisedCosineRollOffFactor", &raisedCosineRollOffFactor);
		addInputParameter("gaussianBandwidthSymbolPeriodProduct", &gaussianBandwidthSymbolPeriodProduct);
		addInputParameter("txPulseShaperLength_symbolPeriods", &txPulseShaperLength_symbolPeriods);
		addInputParameter("MQAMTransmitterSavePulseShapersImpulseResponse", &MQAMTransmitterSavePulseShapersImpulseResponse);
		addInputParameter("MQAMTransmitterSaveInternalSignals", &MQAMTransmitterSaveInternalSignals);
		addInputParameter("MQAMTransmitterLogValue", &MQAMTransmitterLogValue);
		addInputParameter("removeTransmitterPulseShaperFilterDelay", &removeTransmitterPulseShaperFilterDelay);
		addInputParameter("txLocalOscillatorPower_dBm", &txLocalOscillatorPower_dBm);
		addInputParameter("boosterAmplifierGain_dB", &boosterAmplifierGain_dB);
		addInputParameter("boosterAmplifierNoiseFigure_dB", &boosterAmplifierNoiseFigure_dB);
		addInputParameter("boosterAmplifierSaveInternalSignals", &boosterAmplifierSaveInternalSignals);
		addInputParameter("fiberAttenuation_dB", &fiberAttenuation_dB);
		addInputParameter("dispersionCoeficient", &dispersionCoeficient);
		addInputParameter("nlCoeff", &nlCoeff);
		addInputParameter("fiberLength_km", &fiberLength_km);
		addInputParameter("stepLength_m", &stepLength_m);
		addInputParameter("preAmplifierGain_dB", &preAmplifierGain_dB);
		addInputParameter("preAmplifierNoiseFigure_dB", &preAmplifierNoiseFigure_dB);
		addInputParameter("preAmplifierSaveInternalSignals", &preAmplifierSaveInternalSignals);
		addInputParameter("rxLocalOscillatorPower_dBm", &rxLocalOscillatorPower_dBm);
		addInputParameter("rxTiAmplifierImpulseResponseTimeLength_symbolPeriods", &rxTiAmplifierImpulseResponseTimeLength_symbolPeriods);
		//addInputParameter("rxTiAmplifierBandwidth", &rxTiAmplifierBandwidth);
		addInputParameter("rxTiAmplifierGain", &rxTiAmplifierGain);
		addInputParameter("rxReceiverElectricalFilterImpulseResponseType", &rxReceiverElectricalFilterImpulseResponseType);
		addInputParameter("MQAMReceiverSaveInternalSignals", &MQAMReceiverSaveInternalSignals);
	}
};

#endif